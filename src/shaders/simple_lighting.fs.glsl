#version 330

uniform vec3 uViewSpaceLightDir;
uniform vec3 uLightIntensity;

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

out vec3 fColor;

void main()
{
   vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
   float t = dot(uViewSpaceLightDir, viewSpaceNormal);
   fColor = uLightIntensity * t / 3.141592653;
}