#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include <imgui_internal.h>

bool ImGuiAngleWheel(const char* label, float* value_p, float thickness = 2.0f, float radius = 25.0f, int circleNbSegments = 26) {
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	if (window->SkipItems)
		return false;
	//
	ImGuiStyle& style = ImGui::GetStyle();
	float line_height = ImGui::GetTextLineHeight();
	//
	ImVec2 p = ImGui::GetCursorScreenPos();
	ImVec2 center = ImVec2(p.x + radius, p.y + radius);
	// Detect clic
	ImGui::InvisibleButton(label, ImVec2(radius * 2.0f, radius * 2.0f));
	bool is_active = ImGui::IsItemActive();
	bool is_hovered = ImGui::IsItemHovered();

	if (is_active) {
		ImVec2 mp = ImGui::GetIO().MousePos;
		*value_p = atan2f(center.y - mp.y, mp.x - center.x);
	}

	float x2 = cosf(*value_p) * radius + center.x;
	float y2 = -sinf(*value_p) * radius + center.y;

	ImU32 col32 = ImGui::GetColorU32(is_active ? ImGuiCol_FrameBgActive : is_hovered ? ImGuiCol_FrameBgHovered : ImGuiCol_FrameBg);
	ImU32 col32line = ImGui::GetColorU32(ImGuiCol_SliderGrabActive);
	ImU32 col32text = ImGui::GetColorU32(ImGuiCol_Text);
	ImDrawList* draw_list = ImGui::GetWindowDrawList();
	draw_list->AddCircleFilled(center, radius, col32, circleNbSegments);
	draw_list->AddLine(center, ImVec2(x2, y2), col32line, thickness);
	draw_list->AddText(ImVec2(p.x + radius * 2.0f + style.ItemInnerSpacing.y, p.y + radius - line_height * 0.5f), col32text, label);

	return is_active;
}

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
  const auto viewSpaceLightDirLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto lightIntensityLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
  const auto baseColorTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto baseColorFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  // TODO Loading the glTF file
  tinygltf::Model model;
  if (loadGltfFile(model))
    puts("Model successfully loaded !");

  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);
  const auto diagonal = bboxMax - bboxMin;

  // Build projection matrix
  const auto maxDistance = glm::length(diagonal);
  const auto projMatrix = glm::perspective(
    70.f,
    static_cast<float>(m_nWindowWidth) / static_cast<float>(m_nWindowHeight),
    0.001f * maxDistance,
    1.5f * maxDistance
  );

  std::unique_ptr<ICameraController> cameraController = std::make_unique<TrackballCameraController> (
      m_GLFWHandle.window(), 3.f * maxDistance);
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    const auto center = (bboxMin + bboxMax) * 0.5f;
    const auto up = glm::vec3(0, 1, 0);
    const auto eye =
      diagonal.z > 0.000001f ?
      center + diagonal :
      center + 2.f * glm::cross(diagonal, up)  
    ;
    cameraController->setCamera(
        Camera{
          eye,
          center,
          up
        });
  }

  // TODO Creation of Buffer Objects
  auto vboIDs = createBufferObjects(model);

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToRange;
  auto vaoIDs = createVertexArrayObjects(model, vboIDs, meshIndexToRange);

  // Creation of Texture objects
  const auto textureIDs = createTextureObjects(model);
  GLuint whiteTexture;
  // Generate the texture object:
  glGenTextures(1, &whiteTexture);
  // Bind texture object to target GL_TEXTURE_2D:
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  // Set image data:
  float white[] = {1, 1, 1, 1};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0,
          GL_RGB, GL_FLOAT, white);
  // Set interpolation parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  // Set wrapping parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  glBindTexture(GL_TEXTURE_2D, 0);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  const auto bindMaterial = [&](const auto materialIndex) {
    // Get values or default
    GLuint tex = whiteTexture;
    if (materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      glUniform4f(baseColorFactorLocation,
        (float)pbrMetallicRoughness.baseColorFactor[0],
        (float)pbrMetallicRoughness.baseColorFactor[1],
        (float)pbrMetallicRoughness.baseColorFactor[2],
        (float)pbrMetallicRoughness.baseColorFactor[3]);
      if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
        tex = textureIDs[pbrMetallicRoughness.baseColorTexture.index];
      }
    }
    // Apply values
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);
    glUniform1i(baseColorTextureLocation, 0);
    glUniform4f(baseColorFactorLocation, 1.f, 1.f, 1.f, 1.f);
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const auto& node = model.nodes[nodeIdx];
          const glm::mat4 model_matrix = getLocalToWorldMatrix(node, parentMatrix);
          if (node.mesh >= 0) {
            // Matrices
            const glm::mat4 model_view_matrix = viewMatrix * model_matrix;
            const glm::mat4 model_view_projection_matrix = projMatrix * model_view_matrix;
            const glm::mat4 normal_matrix = glm::inverse(glm::transpose(model_view_matrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(model_view_projection_matrix));
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(model_view_matrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normal_matrix));
            const glm::vec3 viewSpaceLightDir = m_bLightFromCamera ? glm::vec3(0, 0, 1) : glm::normalize(
              glm::vec3(
                model_view_matrix * glm::vec4(m_lightDir, 0.f)
              )
            );
            const glm::vec3 lightIntensity = m_lightCol * m_lightIntensity;
            glUniform3f(viewSpaceLightDirLocation, viewSpaceLightDir.x, viewSpaceLightDir.y, viewSpaceLightDir.z);
            glUniform3f(lightIntensityLocation, lightIntensity.x, lightIntensity.y, lightIntensity.z);
            // Mesh and VAO
            const auto& mesh = model.meshes[node.mesh];
            const auto& vaoRange = meshIndexToRange[node.mesh];
            for (size_t primIdx = 0; primIdx < mesh.primitives.size(); ++primIdx) {
              const auto& primitive = mesh.primitives[primIdx];
              bindMaterial(primitive.material);
              glBindVertexArray(vaoIDs[vaoRange.begin + primIdx]);
              if (primitive.indices >= 0) {
                const auto& accessor = model.accessors[primitive.indices];
                const size_t byteOffset = accessor.byteOffset + model.bufferViews[accessor.bufferView].byteOffset;
                glDrawElements(
                  primitive.mode,
                  GLsizei(accessor.count),
                  accessor.componentType,
                  reinterpret_cast<const GLvoid*>(byteOffset)
                );
              }
              else {
                const auto accessorIdx = begin(primitive.attributes)->second;
                const auto& accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }
          for (const auto& childIdx : node.children) {
            drawNode(childIdx, model_matrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      for (auto nodeIdx : model.scenes[model.defaultScene].nodes) {
        drawNode(nodeIdx, glm::mat4(1.f));
      }
    }
  };

  if (!m_OutputPath.empty()) {
    std::vector<unsigned char> pixelData(m_nWindowWidth * m_nWindowHeight * 3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixelData.data(), [&]() {
      drawScene(cameraController->getCamera());
    });
    stbi_flip_vertically_on_write(1);
    stbi_write_png(m_OutputPath.string().c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixelData.data(), 0);
    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
        { // Choose controller type
            const char* items[] = { "Trackball", "First Person" };
            static int item_current = 0;
            if (ImGui::Combo("Camera Type", &item_current, items, IM_ARRAYSIZE(items))) {
              const Camera cam = cameraController->getCamera();
              if (item_current == 0) {
                cameraController = std::make_unique<TrackballCameraController> (
                  m_GLFWHandle.window(), 3.f * maxDistance);
              }
              else {
                cameraController = std::make_unique<FirstPersonCameraController> (
                  m_GLFWHandle.window(), 3.f * maxDistance);
              }
              cameraController->setCamera(cam);
            }
        }
      }
      if (ImGui::CollapsingHeader("Lighting", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::ColorEdit3("Color", glm::value_ptr(m_lightCol), ImGuiColorEditFlags_NoInputs);
        ImGui::SliderFloat("Intensity", &m_lightIntensity, 0.f, 15.f);
        if (ImGuiAngleWheel("Angle Ground", &m_lightAngleGround))
          computeLightDir();
        if (ImGuiAngleWheel("Angle Up", &m_lightAngleUp))
          computeLightDir();
        ImGui::Checkbox("Light From Camera", &m_bLightFromCamera);
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}


bool ViewerApplication::loadGltfFile(tinygltf::Model& model) const {
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());
  //bool ret = loader.LoadBinaryFromFile(&model, &err, &warn, argv[1]); // for binary glTF(.glb)

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
  }

  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(const tinygltf::Model& model) const {
  std::vector<GLuint> vboIDs(model.buffers.size(), 0);
  glGenBuffers(GLsizei(vboIDs.size()), vboIDs.data());
  for (size_t i = 0; i < vboIDs.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, vboIDs[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return vboIDs;
}


std::vector<GLuint> ViewerApplication::createVertexArrayObjects(const tinygltf::Model& model, const std::vector<GLuint>& bufferObjects, std::vector<VaoRange>& meshIndexToVaoRange) const {
  std::vector<GLuint> vaoIDs;
  for (const auto& mesh : model.meshes) {
    const GLsizei begin = GLsizei(vaoIDs.size());
    const GLsizei count = GLsizei(mesh.primitives.size());
    vaoIDs.resize(begin + count);
    meshIndexToVaoRange.emplace_back(begin, count);
    glGenVertexArrays(GLsizei(count), &vaoIDs[begin]);
    for (size_t primitiveIdx = 0; primitiveIdx < mesh.primitives.size(); ++primitiveIdx) {
      const auto& primitive = mesh.primitives[primitiveIdx];
      glBindVertexArray(vaoIDs[begin + primitiveIdx]);
      {
        // POSITION vertex attribute
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != std::end(primitive.attributes)) {
          const auto accessorIdx = iterator->second;
          const auto& accessor = model.accessors[accessorIdx];
          const auto& bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), reinterpret_cast<void*>(byteOffset));
        }
      }
      {
        // NORMAL vertex attribute
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != std::end(primitive.attributes)) {
          const auto accessorIdx = iterator->second;
          const auto& accessor = model.accessors[accessorIdx];
          const auto& bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), reinterpret_cast<void*>(byteOffset));
        }
      }
      {
        // TEXCOORD_0 vertex attribute
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != std::end(primitive.attributes)) {
          const auto accessorIdx = iterator->second;
          const auto& accessor = model.accessors[accessorIdx];
          const auto& bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), reinterpret_cast<void*>(byteOffset));
        }
      }
      if (primitive.indices >= 0) {
        auto bufferView = model.bufferViews[model.accessors[primitive.indices].bufferView];
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferView.buffer]);
      }
    }
  }
  glBindVertexArray(0);
  return vaoIDs;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const {
  // Generate texture objects
  const auto N = model.textures.size();
  std::vector<GLuint> textures(N);
  glGenTextures(static_cast<GLsizei>(N), textures.data());
  // Default sampler
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;
  //
  for (size_t i = 0; i < N; ++i) {
    // Get texture
    const auto& texture = model.textures[i];
    const GLuint texObject = textures[i];
    assert(texture.source >= 0);
    const auto &image = model.images[texture.source];
    // Bind
    glBindTexture(GL_TEXTURE_2D, texObject);
    // Upload data
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
    // Set Sampler
    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);
    // Mipmaps
    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR)
      {
        glGenerateMipmap(GL_TEXTURE_2D);
      }
    // Unbind
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  return textures;
}

void ViewerApplication::computeLightDir() {
  m_lightDir = glm::vec3(
     cos(m_lightAngleUp) * cos(m_lightAngleGround),
    -cos(m_lightAngleUp) * sin(m_lightAngleGround),
    -sin(m_lightAngleUp)
  );
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  computeLightDir();
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);
  glfwSwapInterval(1);

  printGLVersion();
}
