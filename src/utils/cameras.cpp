#include "cameras.hpp"
#include "glfw.hpp"

#include <iostream>

// Good reference here to map camera movements to lookAt calls
// http://learnwebgl.brown37.net/07_cameras/camera_movement.html

using namespace glm;

struct ViewFrame
{
  vec3 left;
  vec3 up;
  vec3 front;
  vec3 eye;

  ViewFrame(vec3 l, vec3 u, vec3 f, vec3 e) : left(l), up(u), front(f), eye(e)
  {
  }
};

ViewFrame fromViewToWorldMatrix(const mat4 &viewToWorldMatrix)
{
  return ViewFrame{-vec3(viewToWorldMatrix[0]), vec3(viewToWorldMatrix[1]),
      -vec3(viewToWorldMatrix[2]), vec3(viewToWorldMatrix[3])};
}

bool FirstPersonCameraController::update(float elapsedTime)
{
  if (glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) &&
      !m_LeftButtonPressed) {
    m_LeftButtonPressed = true;
    glfwGetCursorPos(
        m_pWindow, &m_LastCursorPosition.x, &m_LastCursorPosition.y);
  } else if (!glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) &&
             m_LeftButtonPressed) {
    m_LeftButtonPressed = false;
  }

  const auto cursorDelta = ([&]() {
    if (m_LeftButtonPressed) {
      dvec2 cursorPosition;
      glfwGetCursorPos(m_pWindow, &cursorPosition.x, &cursorPosition.y);
      const auto delta = cursorPosition - m_LastCursorPosition;
      m_LastCursorPosition = cursorPosition;
      return delta;
    }
    return dvec2(0);
  })();

  float truckLeft = 0.f;
  float pedestalUp = 0.f;
  float dollyIn = 0.f;
  float rollRightAngle = 0.f;

  if (glfwGetKey(m_pWindow, GLFW_KEY_W)) {
    dollyIn += 1.f;
  }

  // Truck left
  if (glfwGetKey(m_pWindow, GLFW_KEY_A)) {
    truckLeft += 1.f;
  }

  // Pedestal up
  if (glfwGetKey(m_pWindow, GLFW_KEY_UP)) {
    pedestalUp += 1.f;
  }

  // Dolly out
  if (glfwGetKey(m_pWindow, GLFW_KEY_S)) {
    dollyIn -= 1.f;
  }

  // Truck right
  if (glfwGetKey(m_pWindow, GLFW_KEY_D)) {
    truckLeft -= 1.f;
  }

  // Pedestal down
  if (glfwGetKey(m_pWindow, GLFW_KEY_DOWN)) {
    pedestalUp -= 1.f;
  }

  if (glfwGetKey(m_pWindow, GLFW_KEY_Q)) {
    rollRightAngle -= 0.001f;
  }
  if (glfwGetKey(m_pWindow, GLFW_KEY_E)) {
    rollRightAngle += 0.001f;
  }

  // cursor going right, so minus because we want pan left angle:
  const float panLeftAngle = -0.01f * float(cursorDelta.x);
  const float tiltDownAngle = 0.01f * float(cursorDelta.y);

  const bool hasTranslated = truckLeft || pedestalUp || dollyIn;
  const auto hasMoved = hasTranslated || panLeftAngle || tiltDownAngle || rollRightAngle;

  if (!hasMoved) {
    return false;
  }

  if (hasTranslated) {
    m_camera.moveLocalUniformly(truckLeft, pedestalUp, dollyIn, m_fSpeed * elapsedTime);
  }
  m_camera.rotateLocal(rollRightAngle, tiltDownAngle, 0.f);
  m_camera.rotateWorld(panLeftAngle, m_worldUpAxis);

  return true;
}

bool TrackballCameraController::update(float elapsedTime) {
  if (glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) &&
      !m_MiddleButtonPressed) {
    m_MiddleButtonPressed = true;
    glfwGetCursorPos(
        m_pWindow, &m_LastCursorPosition.x, &m_LastCursorPosition.y);
  } else if (!glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE) &&
             m_MiddleButtonPressed) {
    m_MiddleButtonPressed = false;
  }

  const auto cursorDelta = ([&]() {
    if (m_MiddleButtonPressed) {
      dvec2 cursorPosition;
      glfwGetCursorPos(m_pWindow, &cursorPosition.x, &cursorPosition.y);
      const auto delta = cursorPosition - m_LastCursorPosition;
      m_LastCursorPosition = cursorPosition;
      return delta;
    }
    return dvec2(0);
  })();

  if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_SHIFT)) {
    // Pan
    if (!cursorDelta.x && !cursorDelta.y) {
      return false;
    }
    const auto truckLeft = 0.01f * float(cursorDelta.x);
    const auto pedestalUp = 0.01f * float(cursorDelta.y);

    m_camera.moveLocal(truckLeft, pedestalUp, 0.f);

    return true;
  }

  if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_CONTROL)) {
    // Zoom
    if (cursorDelta.x == 0.) {
      return false;
    }
    float mouseOffset = pow(0.995f, float(cursorDelta.x)); // Use a pow instead of a multiplication since the view vector is then multiplied by this value. We therefore make sure that applying two deltas of 0.1 or one delta of 0.2 is the same.
    const glm::vec3 view = m_camera.eye() - m_camera.center();
    m_camera.setEye(m_camera.center() + mouseOffset * view); // We change the size of the view vector multiplicatively instead of additively so that the zoom is slower when we get close to the center. (And it makes sure that we never get center == eye)

    // todo Implement zoom

    return true;
  }

  // Rotate around target
  if (!cursorDelta.x && !cursorDelta.y) {
    return false;
  }
  const auto longitudeAngle = 0.01f * float(cursorDelta.y); // Vertical angle
  const auto latitudeAngle = -0.01f * float(cursorDelta.x); // Horizontal angle

  const glm::vec3 view = m_camera.eye() - m_camera.center();
  const auto latitudeRotationMatrix = rotate(mat4(1), latitudeAngle, m_worldUpAxis);
  const auto rotationMatrix = rotate(latitudeRotationMatrix, longitudeAngle, m_camera.left());
  const glm::vec3 newView = rotationMatrix * glm::vec4(view, 0.f);
  m_camera.setEye(m_camera.center() + newView);
  m_camera.setUp(rotationMatrix * glm::vec4(m_camera.up(), 0.f));

  // todo Implement rotate

  return true;
}

